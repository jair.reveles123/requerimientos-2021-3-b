requerimientos-2021-3-b

Redacción generalizada
Las historias de usuario deben de ser precisas y detalladas.
La redacción debe de tener las características:


I    Independiente

N    Negociable

V    Valiosa

E    Estimable

S    Pequeña

T    Testeable

Hay que comenzar el desglose de HU a partir de:

Los objetos involucrados en el sistema

Los diferentes perfiles

CRUD (Funcionalidades principales)

Tomó las necesidades generales planteadas por el proyecto y las puso en HU pero no plasma acciones del sistema en sí solo la redacción general.
